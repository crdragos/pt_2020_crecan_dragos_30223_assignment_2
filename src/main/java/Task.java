/**
 * Clasa task este modelarea in cod a clientilor lumii reale.
 */

public class Task implements Comparable<Task> {

    private int arrivalTime;
    private int processingPeriod;
    private int ID;

    /**
     * Constructorul clasei are 3 parametrii prin care sunt intitializate atrubutele clasei cu valorile primitite ca si porametrii.
     * @param arrivalTime
     * @param processingPeriod
     * @param ID
     */

    public Task(int arrivalTime, int processingPeriod, int ID) {
        this.ID = ID;
        this.arrivalTime = arrivalTime;
        this.processingPeriod = processingPeriod;
    }

    /**
     * Se returneaza timpul de procesare al task-ului ce apeleaza metoda.
     */

    public int getProcessingPeriod() {
        return processingPeriod;
    }

    /**
     * In aceasta metoda se va seta valoarea timpului de procesare, folosita la decrementarea acestui timp, odata cu intrarea in coada si trecerea timpului.
     * @param processingPeriod
     */

    public void setProcessingPeriod(int processingPeriod) {
        this.processingPeriod = processingPeriod;
    }

    /**
     * Se returneaza timpul de sosire al fiecarui client.
     * @return
     */

    public int getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Suprascrierea metodei compareTo este necesare deoarece dorim sa sortam clientii dupa timpul de asezare la coda.
     * @param o
     * @return
     */

    @Override
    public int compareTo(Task o) {

        if (this.arrivalTime > o.arrivalTime) {
            return 1;
        }

        if (this.arrivalTime < o.arrivalTime) {
            return -1;
        }

        if (this.arrivalTime == o.arrivalTime) {
            return 0;
        }

        return 0;
    }

    /**
     * Suprascrierea metodei toString, a clasei Object, pentru a pastra un cod curat si a afisa datele conform cerintelor problemei.
     * @return
     */

    @Override
    public String toString() {
        return "(" + this.ID + ", " + this.arrivalTime + ", " + this.processingPeriod + ")";
    }
}
