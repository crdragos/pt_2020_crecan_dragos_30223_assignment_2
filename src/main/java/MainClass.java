/**
 * Clasa Main a programului, contine o instanta a clasei SimulationManager (blocul de control a aplicatiei)
 * In aceasta clasa se pornesc thread-urile aplicatiei, la lansarea in executie a acesteia.
 */

public class MainClass {
    public static void main(String[] args) {

        SimulationManager simulationManager = new SimulationManager(args);
        Thread thread = new Thread(simulationManager);
        thread.start();

    }
}
