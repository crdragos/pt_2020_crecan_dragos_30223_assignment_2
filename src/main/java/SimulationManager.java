import java.io.*;
import java.util.*;

import static java.lang.Thread.sleep;

/**
 * Aceasta clasa este blocul de control al aplicatiei, de aici se pornesc pe rand toate thread-urile.
 */

public class SimulationManager implements Runnable {

    public int timeLimit;
    public int maxProcessingTime;
    public int minProcessingTime;
    public int maxArrivalTime;
    public int minArrivalTime;
    public static int numberOfServers;
    public int numberOfClients;
    private static boolean areThreadsRunning;
    private String outputFile;

    private Scheduler scheduler;

    private List<Task> generatedTasks;

    /**
     * Constructorul clasei primeste ca si parametrii, argumentele date din linia de comanda, deoarece aplicatia va fi lansata in executie sub forma unui fisier .jar
     * @param args
     */

    public SimulationManager(String[] args) {
        readData(new File(args[0]));
        this.scheduler = new Scheduler(this.numberOfServers);
        this.generatedTasks = new ArrayList<Task>();
        this.areThreadsRunning = true;
        this.outputFile = args[1];
        generateNRandomTasks();
    }

    /**
     * Metoda are rolul de a genera random n task-uri si de a le adauga intr-o lista de clienti aflati in asteptare, dupa care acestia vor fi sortati in ordinea timpul de asezare la coada.
     */

    private void generateNRandomTasks() {
        Random random = new Random();
        for (int i = 0; i < this.numberOfClients; i++) {
            int arrivalTime = random.nextInt(maxArrivalTime - minArrivalTime) + minArrivalTime;
            int processingTime = random.nextInt(maxProcessingTime - minProcessingTime) + minProcessingTime;
            Task newTask = new Task(arrivalTime, processingTime, i + 1);
            generatedTasks.add(newTask);
        }
        Collections.sort(generatedTasks);
    }

    /**
     * Thread-ul clasei SimulationManager, principala metoda a clasei, descrisa pe larg in documentatia proiectului.
     */

    @Override
    public void run() {
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(outputFile, false));
        } catch (IOException e) {
            e.printStackTrace();
        }

        int currentTime = 0;
        int clientsAtQueues = 0;
        while (currentTime < this.timeLimit) {
            List<Integer> tasksIndex = new ArrayList<>();
            int iterator = 0;

            for (Task task : this.generatedTasks) {
                if (task.getArrivalTime() == currentTime) {
                    scheduler.dispatchTask(task);
                    tasksIndex.add(iterator);
                    clientsAtQueues++;
                }

                if (clientsAtQueues == this.numberOfClients) {
                    this.timeLimit = Math.min(this.timeLimit, currentTime + scheduler.getMaxTimeAtQueues());
                }

                iterator++;
            }

            for (int index = tasksIndex.size() - 1; index >= 0; index--) {
                generatedTasks.remove(generatedTasks.get(tasksIndex.get(index)));
            }

            writeData(new File(outputFile), currentTime, this.generatedTasks, this.scheduler, false);
            currentTime++;

            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        writeData(new File(outputFile), this.timeLimit, this.generatedTasks, this.scheduler, true);
        areThreadsRunning = false;
    }

    /**
     * Metoda folosita pentru citirea datelor din fisier, primeste ca si parametru fisierul de intrare.
     * @param file
     */

    public void readData(File file) {
        try {
            Scanner input = new Scanner(file);
            this.numberOfClients = input.nextInt();
            this.numberOfServers = input.nextInt();
            this.timeLimit = input.nextInt();
            if (input.hasNextLine()) {
                String[] result = input.next().split(",");
                minArrivalTime = Integer.valueOf(result[0]);
                maxArrivalTime = Integer.valueOf(result[1]);
            }
            if (input.hasNext()) {
                String[] result = input.next().split(",");
                this.minProcessingTime = Integer.valueOf(result[0]);
                this.maxProcessingTime = Integer.valueOf(result[1]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda folosita pentru a scrie datele in fiser, primeste ca si parametrii fisierul de iesire, timpul curent, lista de task-uri, manager-ul si o variablia boolean care decide daca trebuie scris si timpul mediu de asteptare.
     * @param file
     * @param currentTime
     * @param tasks
     * @param scheduler
     * @param printAverageTime
     */

    public void writeData(File file, int currentTime, List<Task> tasks, Scheduler scheduler, boolean printAverageTime) {
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(file, true));

            bufferedWriter.write("Time: " + currentTime);
            bufferedWriter.newLine();

            bufferedWriter.write("Waiting clients: ");
            for (Task task : tasks) {
                bufferedWriter.write(task.toString());
            }
            bufferedWriter.newLine();

            int iterator = 0;
            for (Server server : scheduler.getServers()) {
                bufferedWriter.write("Server " + iterator + ": " + server.toString());
                iterator++;
                bufferedWriter.newLine();
            }
            bufferedWriter.newLine();

            if (printAverageTime) {
                bufferedWriter.write("Average waiting time: " + (float) scheduler.getTotalTime() / this.numberOfClients);
            }

            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Se returneaza variablia care are rolul de a lasa thread-urile active, sau de a le opri in cazul in care toti clienti au fost serviti inainte de limita de timp.
     * @return
     */

    public static boolean getAreThreadsRunning() {
        return areThreadsRunning;
    }
}
