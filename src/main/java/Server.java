import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.sleep;

/**
 * Clasa Server transpune in cod, o coada a lumii reale.
 * Clasa are 2 atribute, o lista de clienti (task-uri) si un timp de asteptare la coada.
 */

public class Server implements Runnable, Comparable<Server> {
    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;

    /**
     * Constructorul clasei
     * Se initializeaza timpul de asteptare la 0, si se creaza o lista goala de clienti.
     */

    public Server() {
        this.tasks = new LinkedBlockingQueue<Task>();
        this.waitingPeriod = new AtomicInteger(0);
    }

    /**
     * In aceasta metoda, se adauga clienti in coada care apeleaza metoda. Clientul este reprezentat de parametrul task.
     * Se adauga clientul in coada si se adauga timpul de procesare al cerintei clientului la timpul de asteptare al cozii.
     * @param task
     */

    public void addTask(Task task) {
        boolean listIsEmplty = false;
        if (this.tasks.size() == 0) {
            listIsEmplty = true;
        }

        this.tasks.add(task);
        this.waitingPeriod.set(this.waitingPeriod.get() + task.getProcessingPeriod());

        if (listIsEmplty) {
            Thread openQueue = new Thread(this);
            openQueue.start();
        }
    }

    /**
     * Acesta este thread-ul clasei.
     */

    @Override
    public void run() {
        while (!this.tasks.isEmpty()) {
            if (!this.tasks.isEmpty()) {
                try {
                    while (this.tasks.element().getProcessingPeriod() != 0) {
                        sleep(1000);
                        this.tasks.element().setProcessingPeriod(this.tasks.element().getProcessingPeriod() - 1);
                        this.waitingPeriod.set(this.waitingPeriod.get() - 1);
                    }
                    Task headOfTheQueue = this.tasks.take();
                    this.waitingPeriod.set(this.waitingPeriod.get() - headOfTheQueue.getProcessingPeriod());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (!SimulationManager.getAreThreadsRunning()) {
                break;
            }
        }
    }

    /**
     * Suprascrierea metodei compareTo pentru a putea sorta cozile in functie de timpul de asteptare.
     * Se compara coada care apeleaza metoda, cu cea care este primita ca si parametru.
     * @param o
     * @return
     */

    @Override
    public int compareTo(Server o) {
        if (this.waitingPeriod.get() > o.waitingPeriod.get()) {
            return 1;
        }

        if (this.waitingPeriod.get() < o.waitingPeriod.get()) {
            return -1;
        }

        if (this.waitingPeriod.get() == o.waitingPeriod.get()) {
            return 0;
        }

        return 0;
    }

    /**
     * Se returneaza timpul de asteptare al cozii.
     * @return
     */

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    /**
     * Suprascrierea metodei toString pentru a afisa datele, conform cerintelor si a avea un clean code.
     * @return
     */

    @Override
    public String toString() {
        if (this.tasks.isEmpty()) {
            return "closed";
        }

        String serverString = "";
        for (Task task : this.tasks) {
            serverString = serverString + task.toString() + " ";
        }

        return serverString;
    }
}
