import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Aceasta clasa este reprezentarea in cod a unui manager / organizator al cozilor
 * Ca si atribute, clasa are o lista de cozi, numarul maxim de cozi si timpul pe care il va avea de astptat un client proaspat pus in coada.
 */

public class Scheduler {

    private List<Server> servers;
    private int maxNoServers;
    private int totalTime;

    /**
     * Constructorul primeste ca si paramentru doar numarul maxim de cozi, pentru a stii cate cozi sa fie create si cate thread-uri sa fie pornite.
     * @param maxNoServers
     */

    public Scheduler(int maxNoServers) {
        this.maxNoServers = maxNoServers;
        this.servers = new ArrayList<Server>();
        this.totalTime = 0;

        for (int i = 0; i < maxNoServers; i++) {
            Server newServer = new Server();
            this.servers.add(newServer);
            Thread serverThread = new Thread(newServer);
            serverThread.start();
        }
    }

//    public void dispatchTask(Task task) {
//        Collections.sort(servers);
//        servers.get(0).addTask(task);
//        for (Server server : servers) {
//            System.out.print(server.getWaitingPeriod() + " ");
//        }
//        System.out.println();
//    }
// asa nu mergea bine, cred ca greseam ceva in gandire, la soratre pare a fi problema

    /**
     * Aceasta metoda primeste ca si parametru un nou task si are rolul de a alege cea mai potrivita coada, conform strategiei alese (momentan, cel mai mic timp de asteptare) si de a distribui task-ul la acea coada.
     * @param task
     */

    public void dispatchTask(Task task) {
        int minTime = Integer.MAX_VALUE;
        int iterator = -1;
        int indexOfMinQueue = -1;

        for (Server s : servers) {
            iterator++;
            if (s.getWaitingPeriod().get() < minTime) {
                minTime = s.getWaitingPeriod().get();
                indexOfMinQueue = iterator;
            }
        }

        totalTime = totalTime + minTime + task.getProcessingPeriod();
        servers.get(indexOfMinQueue).addTask(task);
    }

    /**
     * Se returneaza lista de cozi.
     * @return
     */

    public List<Server> getServers() {
        return this.servers;
    }

    /**
     * Se returneaza timpul total de asteptare, de la toate cozile.
     * @return
     */

    public int getTotalTime() {
        return this.totalTime;
    }

    /**
     * Se returneaza cel mai mare timp de asteprae, de la toate cozile.
     * @return
     */

    public int getMaxTimeAtQueues() {
        int maxTime = Integer.MIN_VALUE;

        for (Server server : this.servers) {
            if (server.getWaitingPeriod().get() > maxTime) {
                maxTime = server.getWaitingPeriod().get();
            }
        }

        return maxTime;
    }
}
